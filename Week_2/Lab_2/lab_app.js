function lab2_task1(num){
    console.log(Math.ceil(num))
}

lab2_task1(5.34)


function lab2_task2(num){
    console.log(Math.floor(num))
}

lab2_task2(5.34)


function lab2_task3(num){
    console.log(Math.round(num))
}

lab2_task3(7.4)


function lab2_task4(num, choice){
    if(choice == "round"){
        console.log(Math.round(num))
    }
    else if(choice == "ceil"){
        console.log(Math.ceil(num))
    }
    else if(choice == "floor"){
        console.log(Math.floor(num))
    }
}

lab2_task4(9.9, "round")


function lab2_task5(){
    console.log(Math.random())
}

lab2_task5


function lab2_task6(){
    console.log(Math.floor(Math.random() * 50) + 5)
}

lab2_task6()


function lab2_task7(a, b){
    console.log((Math.random() * (b - a) + a))
}

lab2_task7(5, 15)


function lab2_task8(a, b){
    console.log(Math.round((Math.random() * (b - a) + a)))
}

lab2_task8(5, 15)


function lab2_task9(a, b){
    for(i = 1; i <= 10; i++){
        document.write(Math.round((Math.random() * (b - a) + a)) + "<br>")
    }
    document.write("<hr>")
}

lab2_task9(5, 40)


function lab2_task10(a, b, n){
    for(i = 1; i <= n; i++){
        document.write(Math.round((Math.random() * (b - a) + a)) + "<br>")
    }
    document.write("<hr>") 
}

lab2_task10(5, 40, 2)


function lab2_task11(){
    weekdays = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"]
    document.write(weekdays[Math.round((Math.random() * 5) + 1)])
}

lab2_task11()


function lab2_task16(){
    console.log(Math.round((Math.random() * 30) + 1))
    document.write("<hr>")
}

lab2_task16()

function lab2_task18(){
    let tb = "<table>"
        for(let i = 0; i < 3; i++){
            tb += "<tr>"
                for(let j = 0; j < 4; j++){
                    let r = Math.ceil(Math.random() * 5)
                    tb += "<td>"
                    tb += "<img src = 'img/" + r + ".jpg'>"
                    tb += "</td>"
                }
            tb += "</tr>"
        }
    tb += "</table>"
    document.write(tb)
}

// lab2_task18()

function lab2_task19(row, col, pic_count){
    let tb = "<table>"
        for(let i = 0; i < row; i++){
            tb += "<tr>"
                for(let j = 0; j < col; j++){
                    let r = Math.ceil(Math.random() * pic_count)
                    tb += "<td>"
                    tb += "<img src = 'img/" + r + ".jpg'>"
                    tb += "</td>"
                }
            tb += "</tr>"
        }
    tb += "</table>"
    document.write(tb)
}

let rows = prompt("Enter rows: ")
let cols = prompt("Enter columns: ")
let pics = prompt("Enter amount of photos: ")
lab2_task19(rows, cols, pics)
