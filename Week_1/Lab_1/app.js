function lab_task_1(text){
    document.write("<i><u><b>" + text)
}

lab_task_1("Sample Text<br><hr>")

function lab_task_2(num1, num2){
    document.write(num1 + num2 + "<hr>")
}

lab_task_2(5, 4)

function lab_task_11(){
    for(i = 1; i <=10; i++){
        document.write(i + " ")
    }
    document.write("<hr>")
}

function lab_task_3(txt, txt_size){
    document.write("<span style = 'font-size: " + txt_size + "px' >" + txt + "</span><hr>")
}

lab_task_3("Sample Text", 60)

lab_task_11()

function lab_task_12(n){
    i = 1
    while(i < n){
        document.write(i + "<br>")
        i++
    }
    document.write("<hr>")
}

lab_task_12(5)

function lab_task_13(m, n){
    while(m < n){
        document.write(m + "<br>")
        m++
    }
    document.write("<hr>")
}

lab_task_12(9, 17)

function lab_task_13(m, n){
    if(m < n){
        while(m < n){
        document.write(m + "<br>")
        m++
    }

    }
    else{
        while(m > n){
        document.write(m + "<br>")
        m--
        }    
    }
    document.write("<hr>")
}

lab_task_13(15, 6)

function lab_task_16(rows, cols, w, h, bg){
    content = "<table class = 'tb-example' style = 'width: " + w + "px; height: " + h + "px; background-color: "+ bg +"'>"
        for(i = 0; i < rows; i++){
            content += "<tr>"
                for(j = 0; j < cols; j++){
                    content += "<td>"
                        content += (i + 1) + "." + (j + 1)
                    content += "</td>"
                }
            content += "</tr>"
        }
    content += "</table>"
    document.write(content)

}

lab_task_16(25, 150, 600, 800, "red")