function tbGenerate(){
    let rowNum = document.getElementById("rowNum")
    let colNum = document.querySelector("#colNum")
    let randNum = document.getElementsByTagName("input")[2]
    let tb = "<table>"
        for(i = 0; i < rowNum.value; i++){
            tb += "<tr>"
                for(j = 0; j < colNum.value; j++){
                    tb += "<td>"
                    tb += "</td>"
                }
            tb += "</tr>"
        }
    tb += "</table>"
    let divTb = document.getElementById("divTb")
    divTb.innerHTML = tb
}


function randGenerate(N){
    let abc = "abcdefghijklmnopqrstuvwxyz"
    let randWrite = ""
    while(randWrite.length != N){
        let symbols = abc[Math.floor(Math.random() * abc.length)]
        if(randWrite.indexOf(symbols) == -1){
            randWrite += symbols 
        }
    }
    console.log(randWrite)
}

randGenerate(5)