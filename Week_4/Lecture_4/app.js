const person = {  //let can be used too
    firstName : "Levan",
    lastName : "Kobulashvili",
    education : {
        Higher : "yes", 
        University : "Georgian American University", 
        GPA : 3.85
    },
    hobbies : ["listening to music", "photography", "vintage stuff"],
    fullName: function(){
        console.log(this.firstName, this.lastName)
    }

}

console.log(person)
console.log(person.firstName)
console.log(person['lastName'])
console.log(person.education.GPA)
console.log(person.hobbies[1])
person.fullName()