var mydivs = document.querySelectorAll("div")
mydivs[0].classList.add("container")
mydivs[1].classList.add("menu")

function spawnCircle(){

    let circ = document.createElement("div")
    circ.classList.add("circle")
    circ.style.backgroundColor = randomColor()
    const diameter = Math.floor((Math.random() * 90) + 10)
    circ.style.height = diameter + "px"
    circ.style.width = diameter + "px"
    circ.style.top = Math.floor(Math.random() * (300 - diameter)) + "px"
    circ.style.left = Math.floor(Math.random() * (800 - diameter)) + "px"
    mydivs[0].insertBefore(circ, mydivs[0].firstChild)

}

function randomColor(){
    const r = Math.floor(Math.random() * 256)
    const g = Math.floor(Math.random() * 256)
    const b = Math.floor(Math.random() * 256)
    return "rgb(" + r + "," + g + "," + b + ")"
}
