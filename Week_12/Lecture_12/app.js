// function start(){
//     setInterval(randomColor(), 1000)
// }

function spawnSquare(){
    let container = document.getElementById("field")
    let num = document.getElementById("inputNumber")
    num = parseInt(num.value)
    container.innerHTML = ""
    if(num >=1 && num <=10){
        for(let i = 0; i < num; i++){
            let square = document.createElement("div")
            square.classList.add("square")
            square.style.backgroundColor = randomColor()
            container.appendChild(square)
        }
    }
}

function randomColor(){
    const r = Math.floor(Math.random() * 256)
    const g = Math.floor(Math.random() * 256)
    const b = Math.floor(Math.random() * 256)
    // return "rgb(" + r + "," + g + "," + b + ")"
    return `rgb(${r}, ${g}, ${b})`
}