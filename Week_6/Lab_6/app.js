function todayDate(){
        const currentTime = new Date()
        const currentYear = currentTime.getFullYear()
        const currentMonth = currentTime.getMonth()
        const currentDate = currentTime.getDate()
        const currentDay = currentTime.getDay()
        const currentHours = currentTime.getHours()
        const currentMinutes = currentTime.getMinutes()
        const currentSeconds = currentTime.getSeconds()
        const currentMilliSeconds = currentTime.getMilliseconds()
        const currentDateElement = document.getElementById("todayTime")
        currentDateElement.textContent = currentYear + "." + currentMonth + "." + currentDate + " - " + currentDay + " - " + currentHours + ":" + currentMinutes + ":" + currentSeconds + ":" + currentMilliSeconds
}

todayDate()


function digitalClock(){
    const currentTime = new Date()
    const currentHours = currentTime.getHours()
    const currentMinutes = currentTime.getMinutes()
    const currentSeconds = currentTime.getSeconds()
    const currentDateElement = document.getElementById("digitalClock")
    currentDateElement.textContent = currentHours + " : " + currentMinutes + " : " + currentSeconds
    setInterval(digitalClock, 1000)
}

digitalClock()


function timeSince2016(){
    const OldTime = new Date("2016-01-01")
    const CurrentTime = new Date()
    const DaysSince = 365 * (CurrentTime.getFullYear() - OldTime.getFullYear())
    const HoursSince = CurrentTime.getHours() - OldTime.getHours()
    const MinutesSince = CurrentTime.getMinutes() - OldTime.getMinutes()
    const SecondsSince = CurrentTime.getSeconds() - OldTime.getSeconds()
    const currentDateElement = document.getElementById("since2016")
    currentDateElement.textContent = DaysSince + " Days, " + HoursSince + " Hours, " + MinutesSince + " Minutes, " + SecondsSince + " Seconds;"
    setInterval(timeSince2016, 1000)
}

timeSince2016()


function geoCalendar(){
    const WeekdaysList = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
    const MonthsList = ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი"]
    const currentTime = new Date()
    const currentYear = currentTime.getFullYear()
    const currentMonth = MonthsList[currentTime.getMonth()]
    const currentDate = currentTime.getDate()
    const currentDay = WeekdaysList[currentTime.getDay()]
    const currentDateElement = document.getElementById("geoCalendar")
    currentDateElement.textContent = currentDay + ", " + currentDate + " " + currentMonth + ", " + currentYear + " წელი"
}

geoCalendar()

const input = prompt("გთხოვთ შეიყვანოთ სასურველი თარიღი რათა გაიგოთ რამდენი დღე და კვირაა გასული ამავე წლის დასაწყისამდე (წწწწ-თთ-დდ):")

function daysSince(){
    const inputDate = new Date(input)
    const year = inputDate.getFullYear()
    const originalDate = new Date(year + "-01-01")
    const daysPassed = Math.floor((inputDate - originalDate) / (1000 * 60 * 60 * 24))
    const currentDateElement = document.getElementById("daysSince")
    currentDateElement.textContent = "თქვენი მითითებული თარიღიდან ამავე წლის დასაწყისამდე გასულია " + daysPassed + " დღე"
}

daysSince()


function weeksSince(){
    const inputDate = new Date(input)
    const year = inputDate.getFullYear()
    const originalDate = new Date(year + "-01-01")
    const weeksPassed = Math.floor((inputDate - originalDate) / (1000 * 60 * 60 * 24 * 7))
    const currentDateElement = document.getElementById("weeksSince")
    currentDateElement.textContent = "თქვენი მითითებული თარიღიდან წლის დასაწყისამდე გასულია " + weeksPassed + " კვირა"
}

weeksSince()