const container = document.getElementById('container')
const square = document.getElementById('square')

function move(direction) {
    const step = 50

    let top = parseInt(square.style.top) || 0
    let left = parseInt(square.style.left) || 0

    switch (direction) {
        case 'reset':
            top = 0
            left = 0
        case 'up':
            top = Math.max(top - step, 0)
            break
        case 'down':
            top = Math.min(top + step, container.clientHeight - square.clientHeight)
            break
        case 'left':
            left = Math.max(left - step, 0)
            break
        case 'right':
            left = Math.min(left + step, container.clientWidth - square.clientWidth)
            break
        case 'upRight':
            top = Math.max(top - step, 0)
            left = Math.min(left + step, container.clientWidth - square.clientWidth)
            break
        case 'upLeft':
            top = Math.max(top - step, 0)
            left = Math.max(left - step, 0)
            break
        case 'downLeft':
            top = Math.min(top + step, container.clientHeight - square.clientHeight)
            left = Math.max(left - step, 0)
            break
        case 'downRight':
            top = Math.min(top + step, container.clientHeight - square.clientHeight)
            left = Math.min(left + step, container.clientWidth - square.clientWidth)
            break
    }

    square.style.top = top + 'px'
    square.style.left = left + 'px'
}

